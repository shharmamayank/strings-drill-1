const convertStringToNumber = (data) => {
    let NumberAfterRemovalOfString = Number(data.replace("$", ''))
    if (isNaN(NumberAfterRemovalOfString)) {
        return 0
    } else {
        return NumberAfterRemovalOfString
    }
}
module.exports = convertStringToNumber