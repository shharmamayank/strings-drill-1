const changeNameIntoTitle = (inventory) => {
    const { first_name, last_name } = inventory
    let first = first_name.toUpperCase().split('')
    let firstLetter = first.slice(0, 1) + first.toString().replace(/,/g, '').slice(1).toLowerCase()
    let last = last_name.toUpperCase().split('')
    let lastNameFirstLetter = last.slice(0, 1) + last.toString().replace(/,/g, '').slice(1).toLowerCase()
    return firstLetter + " " + lastNameFirstLetter
}
module.exports = changeNameIntoTitle